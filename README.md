# Bank Account Transfer

Simple application using spring boot to simulate bank transfer

## Installation

### 1. Build Java Application
```bash
./gradlew build --no-daemon -x test
```
### 2. Run Docker Compose
```bash
docker-compose up
```

## API Documentation
API Usage Spesification

### Account Registration - [POST] /api/auth/register
```bash
{
    "name" : "Beneficiary Name",
    "email" : "beneficiary@mail.com",
    "password" : "123321"
}
```
### Resent Token - [POST] /api/auth/resent-token
```bash
{
    "email" : "mymail@yahoo.com"
}
```

### Verify Token - [GET] /api/auth/verify?token=[token]
###### Query Parameter
```bash
api/auth/verify?token=[token]
```

### Account Login - [POST] /api/auth/login
```bash
{
    "email" : "beneficiary@mail.com",
    "password" : "123321"
}
```
### Account Detail - [GET] /api/account
###### Header
```bash
Bearer [jwt_token]
```

### Update Account Detail - [PUT] /api/account/update
##### Header
```bash
Bearer [jwt_token]
```

##### Body
```bash
{
    "name" : "Rudi",
    "email" : "rudi@mail.com",
    "password" : ""
}
```


### Update Account Pin - [PUT] /api/account/update/pin
##### Header
```bash
Bearer [jwt_token]
```

##### Body
```bash
{
   "oldPin" : "699793",
   "pin" : "123321"
}
```

### Topup Account Balance - [POST] /api/account/topup/balance
##### Header
```bash
Bearer [jwt_token]
```

##### Body
```bash
{
   "amount" : 10000
}
```

### Topup Account Inquiry - [GET] /api/account/inquiry?beneficiaryAccountNo=[accountNo]
##### Header
```bash
Bearer [jwt_token]
```

##### Query Param
```bash
/api/account/inquiry/?beneficiaryAccountNo=[accountNo]
```

### Transfer Balance - [POST] /api/transfer
##### Header
```bash
Bearer [jwt_token]
```

##### Body
```bash
{
    "beneficiaryAccountNo" : "954945",
    "amount" : 100
}
```

### Account Mutation - [GET] /api/transfer/mutation
##### Header
```bash
Bearer [jwt_token]
```