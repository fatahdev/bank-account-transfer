package com.account.bank.enums;

public enum TransactionType {
    CREDIT(1), DEBIT(2);
    public final Integer type;

    private TransactionType(Integer type) {
        this.type = type;
    }
}
