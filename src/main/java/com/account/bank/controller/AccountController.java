package com.account.bank.controller;

import com.account.bank.entities.Account;
import com.account.bank.payloads.request.TopupBalanceRequest;
import com.account.bank.payloads.request.UpdateAccount;
import com.account.bank.payloads.request.UpdateAccountPin;
import com.account.bank.payloads.response.AccountResponse;
import com.account.bank.payloads.response.Response;
import com.account.bank.services.AccountService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

    @GetMapping
    public Response<AccountResponse> account(@AuthenticationPrincipal Account account) {
        AccountResponse accountResponse = accountService.detail(account);
        return Response.<AccountResponse>builder().data(accountResponse).build();
    }

    @PutMapping("/update")
    public Response<AccountResponse> update(@AuthenticationPrincipal Account account, @RequestBody UpdateAccount updateAccount) {
        AccountResponse accountResponse = accountService.update(account, updateAccount);
        return Response.<AccountResponse>builder().data(accountResponse).build();
    }

    @PutMapping("/update/pin")
    public Response<String> updatePin(@AuthenticationPrincipal Account account, @RequestBody UpdateAccountPin updateAccountPin) {
        accountService.updatePin(account, updateAccountPin);
        return Response.<String >builder().data("Pin Updated").build();
    }

    @PostMapping("/topup/balance")
    public Response<String> topupBalance(@AuthenticationPrincipal Account account, @RequestBody TopupBalanceRequest topupBalanceRequest) {
        accountService.topupBalance(account, topupBalanceRequest);
        return Response.<String >builder().data("Success").build();
    }
}
