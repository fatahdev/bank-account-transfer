package com.account.bank.controller;

import com.account.bank.payloads.request.AccountRequest;
import com.account.bank.payloads.request.LoginRequest;
import com.account.bank.payloads.request.ResentTokenRequest;
import com.account.bank.payloads.response.Response;
import com.account.bank.payloads.response.TokenResponse;
import com.account.bank.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    private AuthService authService;


    @PostMapping("/register")
    public Response<TokenResponse> register(@RequestBody AccountRequest accountRequest) {
        TokenResponse otpResponse = authService.register(accountRequest);
        return Response.<TokenResponse>builder().data(otpResponse).build();
    }

    @PostMapping("/resent-token")
    public Response<TokenResponse> resentToken(@RequestBody ResentTokenRequest resentTokenRequest) {
        TokenResponse otpResponse = authService.resentToken(resentTokenRequest);
        return Response.<TokenResponse>builder().data(otpResponse).build();
    }

    @GetMapping("/verify")
    public Response<?> verify(@RequestParam String token) {
        authService.verify(token);
        return Response.builder().data("Verified").build();
    }

    @PostMapping("/login")
    public Response<TokenResponse> login(@RequestBody LoginRequest loginRequest) {
        TokenResponse tokenResponse = authService.login(loginRequest);
        return Response.<TokenResponse>builder().data(tokenResponse).build();
    }
}
