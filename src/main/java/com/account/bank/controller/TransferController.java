package com.account.bank.controller;

import com.account.bank.entities.Account;
import com.account.bank.payloads.request.TransferRequest;
import com.account.bank.payloads.response.InquiryResponse;
import com.account.bank.payloads.response.MutationResponse;
import com.account.bank.payloads.response.Response;
import com.account.bank.payloads.response.TransferResponse;
import com.account.bank.services.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/transfer")
public class TransferController {
    @Autowired
    TransferService transferService;

    @PostMapping
    public Response<TransferResponse> transfer(@AuthenticationPrincipal Account account, @RequestBody TransferRequest transferRequest) {
        TransferResponse transferResponse = transferService.transfer(account, transferRequest);
        return Response.<TransferResponse>builder().data(transferResponse).build();
    }
    
    @GetMapping("/inquiry")
    public Response<InquiryResponse> inquiry(@AuthenticationPrincipal  Account account, @RequestParam String beneficiaryAccountNo) {
        InquiryResponse inquiryResponse = transferService.inquiry(account, beneficiaryAccountNo);
        return Response.<InquiryResponse>builder().data(inquiryResponse).build();
    }

    @GetMapping("/mutation")
    public Response<List<MutationResponse>> mutation(@AuthenticationPrincipal Account account) {
        List<MutationResponse> mutationResponse = transferService.getMutation(account);
        return Response.<List<MutationResponse>>builder().data(mutationResponse).build();
    }
}
