package com.account.bank.services;


import com.account.bank.entities.Account;
import com.account.bank.payloads.request.TransferRequest;
import com.account.bank.payloads.response.InquiryResponse;
import com.account.bank.payloads.response.MutationResponse;
import com.account.bank.payloads.response.TransferResponse;

import java.util.List;

public interface TransferService {
    InquiryResponse inquiry(Account account, String beneficiaryAccountNo);

    TransferResponse transfer(Account account, TransferRequest transferRequest);

    List<MutationResponse> getMutation(Account account);
}
