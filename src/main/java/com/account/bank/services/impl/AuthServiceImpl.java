package com.account.bank.services.impl;

import com.account.bank.config.JwtUtil;
import com.account.bank.entities.Account;
import com.account.bank.payloads.request.AccountRequest;
import com.account.bank.payloads.request.LoginRequest;
import com.account.bank.payloads.request.ResentTokenRequest;
import com.account.bank.payloads.response.TokenResponse;
import com.account.bank.repository.AccountRepository;
import com.account.bank.repository.RedisRepository;
import com.account.bank.security.BCrypt;
import com.account.bank.services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Random;
import java.util.UUID;

@Service
public class AuthServiceImpl implements AuthService {
    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private RedisRepository redisRepository;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public TokenResponse register(AccountRequest accountRequest) {
        try {
            if (accountRepository.existsByEmail(accountRequest.getEmail())) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Email already exist !");
            }

            Account account = new Account();
            account.setName(accountRequest.getName());
            account.setEmail(accountRequest.getEmail());
            account.setPin(this.generatePin().toString());
            account.setAccountNo(this.generateAccountNo().toString());
            account.setBalance(0);
            account.setPassword(BCrypt.hashpw(accountRequest.getPassword(), BCrypt.gensalt()));

            Account savedAccount = accountRepository.saveAndFlush(account);

            String otp = this.generateOtp().toString();
            redisRepository.saveOtp(otp, savedAccount);

            return TokenResponse.builder().token(otp).build();
        } catch (Exception e) {
            throw e;
        }
    }

    @Override
    public TokenResponse resentToken(ResentTokenRequest resentTokenRequest) {
        Account account = accountRepository.getUserByEmail(resentTokenRequest.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account doesn't exist !"));

        if (account.getIsVerified() != null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account doesn't exist !");
        }

        String otp = this.generateOtp().toString();
        redisRepository.saveOtp(otp, account);

        return TokenResponse.builder().token(otp).build();
    }

    @Override
    public void verify(String token) {
        try {
            Account account = redisRepository.getOtp(token);

            if (account == null) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Token");
            }

            Account findAccount = accountRepository.getUserByEmail(account.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account doesn't exist !"));
            findAccount.setIsVerified((short) 1);

            accountRepository.saveAndFlush(findAccount);
        } catch (Exception e) {
            throw  e;
        }
    }

    @Override
    public TokenResponse login(LoginRequest loginRequest) {
        try {
            Account account = accountRepository.getUserByEmail(loginRequest.getEmail()).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Account doesn't exist !"));

            if (account.getIsVerified() == null) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Email or password is wrong");
            }

            if (!BCrypt.checkpw(loginRequest.getPassword(), account.getPassword())) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Email or password is wrong");
            }

            String token = jwtUtil.generateToken(account.getId());

            return TokenResponse.builder().token(token).build();
        } catch (Exception e) {
            throw  e;
        }
    }

    private Integer generatePin() {
        return new Random().nextInt(1000000);
    }

    private Integer generateAccountNo() {
        return new Random().nextInt(1000000);
    }
    private Integer generateOtp() {
        return new Random().nextInt(1000000);
    }
}
