package com.account.bank.services.impl;


import com.account.bank.entities.Account;
import com.account.bank.entities.Mutation;
import com.account.bank.enums.TransactionType;
import com.account.bank.payloads.request.TransferRequest;
import com.account.bank.payloads.response.InquiryResponse;
import com.account.bank.payloads.response.MutationResponse;
import com.account.bank.payloads.response.TransferResponse;
import com.account.bank.repository.AccountRepository;
import com.account.bank.repository.MutationRepository;
import com.account.bank.repository.RedisRepository;
import com.account.bank.services.TransferService;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransferServiceImpl implements TransferService {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    RedisRepository redisRepository;

    @Autowired
    MutationRepository mutationRepository;

    @Override
    public InquiryResponse inquiry(Account account, String beneficiaryAccountNo) {
        if (beneficiaryAccountNo.isBlank() || beneficiaryAccountNo.isEmpty()) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "Beneficiary Account Invalid");
        }

        if (account.getAccountNo().equals(beneficiaryAccountNo)) {
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "Beneficiary Account Invalid");
        }

        Account beneficiaryAccount = accountRepository.getUserByAccountNo(beneficiaryAccountNo).orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Beneficiary Account Invalid"));

        return InquiryResponse.builder().name(beneficiaryAccount.getName()).build();
    }

    @Override
    @Transactional
    public TransferResponse transfer(Account account, TransferRequest transferRequest) {
        Account beneficiaryAccount = accountRepository
                .getUserByAccountNo(transferRequest.getBeneficiaryAccountNo())
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST, "Beneficiary Account Invalid"));

        Integer totalAmountDebit = account.getBalance() - transferRequest.getAmount();
        account.setBalance(totalAmountDebit);
        accountRepository.saveAndFlush(account);

        Mutation mutationDebit = new Mutation();
        mutationDebit.setAmount(transferRequest.getAmount());
        mutationDebit.setAccountNo(account.getAccountNo());
        mutationDebit.setTransactionType(TransactionType.DEBIT);
        mutationDebit.setBeneficiaryAccountNo(transferRequest.getBeneficiaryAccountNo());
        mutationRepository.saveAndFlush(mutationDebit);

        Mutation mutationCredit = new Mutation();
        mutationCredit.setAmount(transferRequest.getAmount());
        mutationCredit.setAccountNo(beneficiaryAccount.getAccountNo());
        mutationCredit.setTransactionType(TransactionType.CREDIT);
        mutationRepository.saveAndFlush(mutationCredit);

        Integer totalAmountCredit = beneficiaryAccount.getBalance() + transferRequest.getAmount();
        beneficiaryAccount.setBalance(totalAmountCredit);
        accountRepository.saveAndFlush(beneficiaryAccount);

        return TransferResponse.builder().amount(transferRequest.getAmount())
                .beneficiaryAccountNo(transferRequest.getBeneficiaryAccountNo())
                .name(beneficiaryAccount.getName())
                .build();
    }

    @Override
    public List<MutationResponse> getMutation(Account account) {
        List<MutationResponse> response = new ArrayList<>();

        List<Mutation> mutation = mutationRepository.getMutationByAccountNoOrBeneficiaryAccountNo(account.getAccountNo(), account.getAccountNo());

        mutation.stream().forEach(item ->
                response.add(new MutationResponse(item.getTransactionType().toString(), item.getAccountNo(), item.getBeneficiaryAccountNo(), item.getAmount()))
        );
        
        return response;
    }
}
