package com.account.bank.services.impl;

import com.account.bank.entities.Account;
import com.account.bank.entities.Mutation;
import com.account.bank.enums.TransactionType;
import com.account.bank.payloads.request.TopupBalanceRequest;
import com.account.bank.payloads.request.UpdateAccount;
import com.account.bank.payloads.request.UpdateAccountPin;
import com.account.bank.payloads.response.AccountResponse;
import com.account.bank.payloads.response.MutationResponse;
import com.account.bank.repository.AccountRepository;
import com.account.bank.repository.MutationRepository;
import com.account.bank.security.BCrypt;
import com.account.bank.services.AccountService;

import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.Objects;

@Service
public class AccountServiceImpl implements AccountService {
    @Autowired
    AccountRepository accountRepository;

    @Autowired
    MutationRepository mutationRepository;

    @Override
    public void updatePin(Account account, UpdateAccountPin updateAccountPin) {
        if (!account.getPin().equals(updateAccountPin.getOldPin())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Old pin not match !");
        }

        account.setPin(updateAccountPin.getPin());
        accountRepository.saveAndFlush(account);
    }

    @Override
    public AccountResponse update(Account account, UpdateAccount updateAccount) {

        if (Objects.nonNull(updateAccount.getName())) {
            account.setName(updateAccount.getName());
        }

        if (Objects.nonNull(updateAccount.getEmail())) {
            account.setEmail(updateAccount.getEmail());
        }

        if (Objects.nonNull(updateAccount.getPassword()) && !updateAccount.getPassword().isBlank() && !updateAccount.getPassword().isEmpty() ) {
            account.setPassword(BCrypt.hashpw(updateAccount.getPassword(), BCrypt.gensalt()));
        }

        accountRepository.saveAndFlush(account);

        return AccountResponse.builder().id(account.getId())
                .accountNo(account.getAccountNo())
                .email(account.getEmail())
                .name(account.getName())
                .balance(account.getBalance())
                .pin(account.getPin())
                .isVerified(account.getIsVerified())
                .build();
    }

    @Override
    public AccountResponse detail(Account account) {
        return AccountResponse.builder().id(account.getId())
                .accountNo(account.getAccountNo())
                .email(account.getEmail())
                .name(account.getName())
                .balance(account.getBalance())
                .pin(account.getPin())
                .isVerified(account.getIsVerified())
                .build();
    }

    @Override
    @Transactional
    public void topupBalance(Account account, TopupBalanceRequest topupBalanceRequest) {
        if (Objects.nonNull(topupBalanceRequest.getAmount()) && topupBalanceRequest.getAmount() == 0) {
          throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Amount !");
        }

        account.setBalance(topupBalanceRequest.getAmount());
        accountRepository.saveAndFlush(account);

        Mutation mutation = new Mutation();
        mutation.setAccountNo(account.getAccountNo());
        mutation.setTransactionType(TransactionType.CREDIT);
        mutation.setAmount(topupBalanceRequest.getAmount());

        mutationRepository.save(mutation);
    }

    @Override
    public Account referenceById(Long accountId) {
        return accountRepository.getReferenceById(accountId);
    }
}
