package com.account.bank.services.impl;

import com.account.bank.entities.Account;
import com.account.bank.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomAccountDetailServiceImpl implements UserDetailsService {

    @Autowired
    AccountRepository accountRepository;
    @Override
    public UserDetails loadUserByUsername(String accountId) throws UsernameNotFoundException {
        Account account = accountRepository.getReferenceById(Long.valueOf(accountId));
        return User.builder()
                        .username(account.getId().toString())
                        .password(account.getPassword())
                        .build();
    }
}
