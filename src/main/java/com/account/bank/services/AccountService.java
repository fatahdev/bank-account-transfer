package com.account.bank.services;

import com.account.bank.entities.Account;
import com.account.bank.entities.Mutation;
import com.account.bank.payloads.request.TopupBalanceRequest;
import com.account.bank.payloads.request.UpdateAccount;
import com.account.bank.payloads.request.UpdateAccountPin;
import com.account.bank.payloads.response.AccountResponse;

public interface AccountService {

    void updatePin(Account account, UpdateAccountPin updateAccountPin);

    AccountResponse update(Account account, UpdateAccount updateAccount);

    AccountResponse detail(Account account);

    void topupBalance(Account account, TopupBalanceRequest topupBalanceRequest);

    Account referenceById(Long accountId);
}
