package com.account.bank.services;

import com.account.bank.payloads.request.AccountRequest;
import com.account.bank.payloads.request.LoginRequest;
import com.account.bank.payloads.request.ResentTokenRequest;
import com.account.bank.payloads.response.TokenResponse;

public interface AuthService {
    TokenResponse register(AccountRequest accountRequest);

    TokenResponse resentToken(ResentTokenRequest resentTokenRequest);


    void verify(String token);

    TokenResponse login(LoginRequest loginRequest);
}
