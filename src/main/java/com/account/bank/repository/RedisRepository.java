package com.account.bank.repository;

import com.account.bank.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.concurrent.TimeUnit;

@Repository
public class RedisRepository {
    @Autowired
    private RedisTemplate redisTemplate;

    public void saveOtp(String otp, Account account) {
        redisTemplate.opsForValue().set(otp, account, 60, TimeUnit.SECONDS);
    }

    public Account getOtp(String otp) {
        return (Account) redisTemplate.opsForValue().getAndDelete(otp);
    }
}
