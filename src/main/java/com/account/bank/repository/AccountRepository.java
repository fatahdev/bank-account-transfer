package com.account.bank.repository;

import com.account.bank.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {
    Boolean existsByEmail(String email);

    Account findByEmail(String email);
    Optional<Account> getUserByEmail(String email);

    Optional<Account> getUserByAccountNo(String accountNo);
}
