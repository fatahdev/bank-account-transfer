package com.account.bank.repository;

import com.account.bank.entities.Mutation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MutationRepository extends JpaRepository<Mutation, Long> {
    List<Mutation> getMutationByAccountNoOrBeneficiaryAccountNo(String accountNo, String beneficiaryAccountNo);
}
