package com.account.bank.payloads.response;


import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Response<T> {
    private T data;

    private String errors;
}
