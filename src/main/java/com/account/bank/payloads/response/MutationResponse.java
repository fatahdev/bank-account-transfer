package com.account.bank.payloads.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MutationResponse {
    private String transactionType;
    private String sourceAccountNo;
    private String beneficiaryAccountNo;
    private Integer amount;
}
