package com.account.bank.payloads.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class ResentTokenRequest {

    @NotEmpty
    @Email(message = "Invalid Email address, Please Input valid Email!")
    private String email;
}
