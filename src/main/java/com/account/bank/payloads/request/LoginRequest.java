package com.account.bank.payloads.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class LoginRequest {
    @Email(message = "Invalid Email address, Please Input valid Email!")
    private String email;

    @NotEmpty
    @Size(min = 3, max = 6, message = "Input pin min 3 and max 6 Characters")
    private String password;
}
