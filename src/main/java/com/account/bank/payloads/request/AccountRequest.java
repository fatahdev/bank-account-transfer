package com.account.bank.payloads.request;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import jakarta.validation.constraints.Size;

@NoArgsConstructor
@Getter
@Setter
public class AccountRequest {
    private int id;

    @Size(min = 4, message = "Username atleast 4 character long!")
    private String name;

    @Email(message = "Invalid Email address, Please Input valid Email!")
    private String email;

    @NotEmpty
    @Size(min = 3, max = 6, message = "Input pin min 3 and max 6 Characters")
    private String password;
}
