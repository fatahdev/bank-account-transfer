CREATE TABLE IF NOT EXISTS mutations (
  id BIGSERIAL PRIMARY KEY,
  account_no VARCHAR(10),
  beneficiary_account_no VARCHAR(10),
  transaction_type smallint,
  is_verified smallint,
  amount integer,
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
