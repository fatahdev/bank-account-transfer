CREATE TABLE IF NOT EXISTS accounts (
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(100),
  password VARCHAR(100),
  pin VARCHAR(6),
  account_no VARCHAR(10),
  is_verified smallint,
  balance integer NOT NULL DEFAULT(0),
  updated_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  created_at timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
);
