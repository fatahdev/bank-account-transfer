#FROM openjdk:21-jdk-slim as builder
#RUN mkdir -p /app
#COPY . ./app
#WORKDIR /app
#CMD ["./gradlew","build","--no-daemon","-x","test"]

FROM openjdk:21-jdk-slim
COPY ./build/libs/bank-0.0.1-SNAPSHOT.jar app.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "app.jar"]